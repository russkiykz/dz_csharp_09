﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackupCopy
{
    public class Flash : Storage
    {
        public double SpeedUSB_v3 { get; set; }
        public double MemorySize { get; set; }
        
        public Flash()
        {
            Name = "Flash";
            Model = "Trancent";
            SpeedUSB_v3 = 5120;
        }

        public override double AmountOfMemory()
        {
            return MemorySize;
        }

        public override void CopyingData(int[] data)
        {
            int countOfFiles = 0 ;
            Console.WriteLine("Начало копирования файлов");
            FreeMemoryDevice = MemorySize;
            for (int i = 0; i < data.Length; i++)
            {
                FreeMemoryDevice -= data[i];
                if (FreeMemoryDevice < data[i])
                {
                    Console.WriteLine($"Скопировано {i+1} файлов\nНе достаточно места для копирования остальных файлов");
                    break;
                }
                countOfFiles++;
            }
            Console.WriteLine($"Копирование завершено\nСкопировано {countOfFiles} файлов");
        }

        public override void DeviceInfo()
        {
            Console.WriteLine($"\n{Name} - {Model}\nСкорость USB 3.0 - {SpeedUSB_v3/1024} Гб/с\nОбъем памяти: {MemorySize/1024} Гб");
        }

        public override void FreeMemory()
        {
            Console.WriteLine($"\nFlash\nСвободный объем: {(FreeMemoryDevice)/1024} Гб");
        }
    }
}

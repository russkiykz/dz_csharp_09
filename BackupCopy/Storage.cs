﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackupCopy
{
    public abstract class Storage
    {
        protected string Name { get; set; }
        protected string Model { get; set; }
        protected double FreeMemoryDevice { get; set; }

        public abstract double AmountOfMemory();
        public abstract void CopyingData(int[] data);
        public abstract void FreeMemory();
        public abstract void DeviceInfo();
    }
}

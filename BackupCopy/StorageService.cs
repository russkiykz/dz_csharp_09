﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackupCopy
{
    public abstract class StorageService
    {
        public static void TotalMemoryInfo(Storage[] storages)
        {
            double totalMemory = 0;
            foreach(Storage storage in storages)
            {
                totalMemory += storage.AmountOfMemory();
            }
            Console.WriteLine($"\nОбщий объем памяти устройств: {(int)(totalMemory/1024)} Гб");
        }

        public static Storage[] AddDeviceInformation(Storage[] storages)
        {
            foreach (Storage storage in storages)
            {
                if (storage is Flash)
                {
                    Console.Write("\nFlash\nОбъем памяти(Гб): ");
                    double.TryParse(Console.ReadLine(), out double memory);
                    (storage as Flash).MemorySize = memory*1024;
                }
                else if (storage is DVD)
                {
                    Console.Write("\nDVD\nВыберите тип DVD-диска:\n1.Односторонний (4.7 Гб)\n2.Двусторонний (9 Гб)\nВыбор: ");
                    switch (Console.ReadLine())
                    {
                        case "1":
                            (storage as DVD).Type = 4.7*1024;
                            break;
                        case "2":
                            (storage as DVD).Type = 9*1024;
                            break;
                    }
                }
                else if (storage is HDD)
                {
                    Console.Write("\nHDD\nКоличество разделов:");
                    int.TryParse(Console.ReadLine(), out int numberOfSections);
                    Console.Write("\nОбъем разделов(Гб): ");
                    double.TryParse(Console.ReadLine(), out double volumeOfSections);
                    (storage as HDD).NumberOfSections = numberOfSections;
                    (storage as HDD).VolumeOfSections = volumeOfSections*1024;
                }
            }
            return storages;
        }
    }
}

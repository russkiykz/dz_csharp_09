﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackupCopy
{
    public class DVD : Storage
    {
        public double WriteSpeed { get; set; }
        public double Type { get; set; }

        public DVD()
        {
            Name = "DVD-RW";
            Model = "Mirex";
            WriteSpeed = 2.4;
        }

        public override double AmountOfMemory()
        {
            return Type;
        }

        public override void CopyingData(int[] data)
        {
            int countOfFiles = 0;
            Console.WriteLine("Начало копирования файлов");
            FreeMemoryDevice = Type;
            for (int i = 0; i < data.Length; i++)
            {
                FreeMemoryDevice -= data[i];
                if (FreeMemoryDevice < data[i])
                {
                    Console.WriteLine($"Скопировано {i + 1} файлов\nНе достаточно места для копирования остальных файлов");
                    break;
                }
                countOfFiles++;
            }
            Console.WriteLine($"Копирование завершено\nСкопировано {countOfFiles} файлов");
        }

        public override void DeviceInfo()
        {
            Console.WriteLine($"\n{Name} - {Model}\nСкорость чтения/записи - {WriteSpeed} Мб/с\nТип (односторонний (4.7 Гб) /двусторонний (9 Гб): {Type/1024} Гб");
        }

        public override void FreeMemory()
        {
            Console.WriteLine($"\nDVD\nСвободный объем: {(FreeMemoryDevice) / 1024} Гб");
        }
    }
}

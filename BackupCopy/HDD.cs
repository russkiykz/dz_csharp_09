﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackupCopy
{
    public class HDD : Storage
    {
        public double SpeedUSB_v2 { get; set; }
        public int NumberOfSections { get; set; }
        public double VolumeOfSections { get; set; }

        public HDD()
        {
            Name = "HDD";
            Model = "Western Digital";
            SpeedUSB_v2 = 480;
        }

        public override double AmountOfMemory()
        {
            return NumberOfSections * VolumeOfSections;
        }

        public override void CopyingData(int[] data)
        {
            int countOfFiles = 0;
            Console.WriteLine("Начало копирования файлов");
            FreeMemoryDevice = NumberOfSections * VolumeOfSections;
            for (int i = 0; i < data.Length; i++)
            {
                FreeMemoryDevice -= data[i];
                if (FreeMemoryDevice < data[i])
                {
                    Console.WriteLine($"Скопировано {i + 1} файлов\nНе достаточно места для копирования остальных файлов");
                    break;
                }
                countOfFiles++;
            }
            Console.WriteLine($"Копирование завершено\nСкопировано {countOfFiles} файлов");
        }

        public override void DeviceInfo()
        {
            Console.WriteLine($"\n{Name} - {Model}\nСкорость USB 2.0  - {SpeedUSB_v2} Мб/с\nКоличество разделов: {NumberOfSections}\nОбъем разделов: {VolumeOfSections/1024} Гб");
        }

        public override void FreeMemory()
        {
            Console.WriteLine($"\nHDD\nСвободный объем: {(FreeMemoryDevice) / 1024} Гб");
        }
    }
}

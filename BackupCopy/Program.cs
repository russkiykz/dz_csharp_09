﻿using System;

namespace BackupCopy
{
    class Program
    {
        static void Main(string[] args)
        {
            Storage[] devices = new Storage[3] { new Flash(), new DVD(), new HDD() };
            StorageService.AddDeviceInformation(devices);
            Console.Clear();
            (devices[0] as Flash).DeviceInfo();
            (devices[1] as DVD).DeviceInfo();
            (devices[2] as HDD).DeviceInfo();

            // Общее кол-во памяти
            StorageService.TotalMemoryInfo(devices);

            // Копирование файлов на устройство
            (devices[0] as Flash).CopyingData(new int[] { 1024, 1024, 1024, 1024, 1024 });
            (devices[0] as Flash).FreeMemory();

        }
    }
}
